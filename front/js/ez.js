var EZ = EZ || {};

EZ.Main = EZ.Main|| {

    prepare : function() {
        $('.hidden-cell').hide();
        $('.details').show();
    },

    disableRemoveSelectionButton : function() {
        $('.removeSelection').attr('disabled', true);
    },

    enableRemoveSelectionButton : function() {
        $('.removeSelection').attr('disabled', false);
    },

    showHideDetails : function(obj) {
        var id = $(obj).attr('id');
        if ($('#show' + id).length) {
            $('#show' + id).remove();
            return;
        }
        var comments = $(obj).closest('tr').find('.cell-comments').text(),
            updated = $(obj).closest('tr').find('.cell-updated').text(),
            section = $(obj).closest('tr').find('.cell-section').text();
        var content = "<b>Comments:</b> " + comments
                + " <b>Updated:</b> " + updated
                + " <b>Section:</b>" + section;
        $('<tr id="show' + id + '"><td colspan="10">' + content + '</td></tr>').insertAfter($(obj).closest('tr'));

    },

    hookEvents : function() {
        var self = this;
        $('.content-list').on('click', '.check', function(){
            $(this).closest('tr').toggleClass('info');
            if ($( ".check:checkbox:checked" ).length) {
                EZ.Main.enableRemoveSelectionButton();
            } else {
                EZ.Main.disableRemoveSelectionButton();
            }
        }).on('click', '.show-details', function(e){
            e.preventDefault();
            EZ.Main.showHideDetails(this);
        });
    }

};

$(document).ready(function () {
    EZ.Main.prepare();
    EZ.Main.disableRemoveSelectionButton();
    EZ.Main.hookEvents();
    console.log(":)");
});