<?php
/**
 * Created by PhpStorm.
 * User: Kamil Musial
 * Date: 2014-12-22
 * Time: 20:05
 */

session_start();

interface LoadBalancerInterface {

    /**
     * @param Request $request
     * @return request
     */
    public function handleRequest(Request $request);

} 

class LoadBalancer implements LoadBalancerInterface {


    /**
     * @param array $instances array of instances
     * @param string $algorithm name of algorithm
     */
    public function __construct(array $instances, string $algorithm) {
        if ( ! count($instances)) {
            throw new Exception("No instances", 1);
        }
        $this->instances = $instances;

        // Instead of throwing errors, use rotation algorithm
        $algorithms = array("rotationAlgorithm", "lowestLoadAlgorithm");
        if ( ! in_array($algorithm, $algorithms)) {
            $algorithm = "rotationAlgorithm";
        }
        // Instances passed to algorithm constructor if manipulated in the future
        $this->algorithm = new $algorithm($instances);
    }

    public function handleRequest(Request $request) {
        $host = $this->algorithm->selectHost();
        return $host->handleRequest($request);    
    }
} 

abstract class LBAlgorithm {

    public $instances;

    /**
     * @return instance return instance object
     */
    abstract public function selectHost();
}

class rotationAlgorithm extends LBAlgorithm {

    public $lastHost;

    /**
     * @param array $instances array of instances
     */
    public function __construct(array $instances) {
        $this->instances = $instances;
        $this->lastHost = (isset($_SESSION['lastHost'])) ? $_SESSION['lastHost'] : 0;
    }

    public function selectHost() {
        $nextElement = $this->lastHost + 1;
        if (isset($this->instances[$nextElement])) {
            $nextHost = $this->instances[$nextElement];
            $_SESSION['lastHost'] = $nextElement;
        } else {
            $nextHost = $this->instances[0];
            $_SESSION['lastHost'] = 0;
        }
        
        return $nextHost;
    }
} 

class lowestLoadAlgorithm extends LBAlgorithm {

    /**
     * @param array $instances array of instances
     */
    public function __construct($instances) {
        $this->instances = $instances;
    }

    public function selectHost() {
        $lastLoad = 0;
        $lowest = $this->instances[0];

        foreach ($this->instances as $instance) {
            $load = $instance->getLoad();
            if ($load < 0.75) {
                return $instance;
            } else {
                $lowest = ($load < $lastLoad) ? $instance : $lowest;
                $lastLoad = $load;
            }
        }

        return $lowest;
    }
} 
